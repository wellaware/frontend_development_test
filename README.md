## Quick Read

Please clone this repository using [git](http://git-scm.com/) and complete the problem described below in that repository. We prefer that you do not fork this repository; just clone it and commit your work to your local copy of the repository. If you are not familiar with git, you can find several guides online.

This repo also contains the source code to the API server from which you can hit. This is also hosted on a cloud server if you do not wish to launch your own local version for testing. It can be found at: http://wellaware-frontend-dev-test.herokuapp.com/ You may elect to remove the server source code from this repo if you intend to utilize our hosted version.

If you have an additional IDE files or folders created please add them to your `.gitignore` or specifically do not add them into the git repo.

When you start, please create the file called `started` that contains the current time at which you started. Please use RFC-3339 format with seconds level precision. Then commit this file only with the commit message of `Starting Challenge`

When you have completed the problem and commited your work to your local repository, zip the resulting directory and return the zipped solution to us by email. If the zip file is too large for email, you may host it using any storage solution you prefer (Dropbox, Google, Microsoft, ...) and send us the link.

## Overview

These instructions apply to both the web developer and the mobile application developer.

You are to write a simple mobile application or web application to implement an asset map view, with detailed asset views and data point graphing. The application should query the API server (with the appropriate credentials) for a list of available sites. These sites should be loaded into a map view. When a user clicks on the site it should take them into a zoomed map view with the associated asset endpoints available. When a user clicks on the associated asset endpoint, a tooltip should show the current production totals.

At this point the requirements deviate for web and mobile:

* The web version should allow the user to click on the tooltip to drop into a detailed graph view of past data. An API endpoint exists to generate graph data for you.
* The mobile version should allow filtering of assets by type via on/off toggle switches.  You are free to place the filter switches wherever you see fit but please match the style shown in the mockup below. Bonus points if you able to get the mobile version to do graphing.

![Filtering Toggle Switch Style](https://bytebucket.org/wellaware/frontend_development_test/raw/ba90401e1bf9b9277f1e5f8b219ec20ab00feeb4/mockup.png "Filtering Toggle Switch Style")

## Data

Sites are groups of assets, much like a neighborhood contains many houses.

Assets are the physical representation of an object (or a house in the neighborhood model), for us there are two types of assets. Tanks and Christmas trees (not the Decemeber 25th kind).  A Tank can only hold Oil and/or Water, while a well christmas tree from the persepective of the customer is interested only in gas production.

## Evaluation Criteria

One of the most important criteria for a successful solution is that it correctly implements the problem to be solved. This will be determined by code inspection, acceptance testing using a smoke test for all the possible views, as well as additional test cases not provided on this page.

We will also be considering your overall approach to the problem and your programming style. This assignment is an opportunity to show off how you can use your object-oriented analysis and design skills to produce an elegant, readable, and maintainable solution. Please DO use what you would consider best practice in developing your solution. Please also be aware that we will be simulating high network latency problems for mobile application applicants as this is a real common problem in this area - do what you would consider best practice for handling poor network conditions.

## Deliverables (all of the following)

The following should be available in your resulting solution zip file:

 1. Source Code - The source code for mobile applications should be in their respective language, the web application should be HTML5/JavaScript.  Mobile applications should be native and not web views, etc.  Web applications should use JSONP to access the API.  Include unit tests for web applications as appropriate. Use the respective mobile application test suites.
 2. Any other supporting code or other binaries that you used to develop the solution (even if it is not required to run the solution). If a resource is easily accessible on the web you can just provide a link to it rather than including the files.

## API Endpoints
The hosted API server is running at http://wellaware-frontend-dev-test.herokuapp.com/

| EndPoint URL | Description | Parameter |
| ------------ | ----------- | --------- |
| /sites | Retrieve all the Sites and their associated Assets Endpoints | None |
| /site/`endpoint_id` | Retrieve the Asset Endpoint Information | `endpoint_id` - Integer |
| /site/`endpoint_id`/data | Retrieve the raw historical data for an endpoint | `endpoint_id` - Integer |


## Output API Format

All responses are in *JSON*. 

Review the below examples for sample input and output.

### Example #1 - Retrieve all Sites
Retreiving all available site information.

`curl -i http://wellaware-frontend-dev-test.herokuapp.com/sites`

Response:
```
{
  "sites": [
    {
      "endpoints": [
        {
          "latitude": 29.603547, 
          "longitude": -98.529522, 
          "name": "Well Tree", 
          "pk": 0, 
          "type": "XMASTREE"
        }, 
        {
          "latitude": 29.603376, 
          "longitude": -98.529973, 
          "name": "Tank 1", 
          "pk": 1, 
          "type": "TANK"
        }, 
        {
          "latitude": 29.603382, 
          "longitude": -98.530053, 
          "name": "Tank 2", 
          "pk": 2, 
          "type": "TANK"
        }
      ], 
      "latitude": 29.60376, 
      "longitude": -98.529818, 
      "name": "Alice Well", 
      "pk": 0
    }, 
    {
      "endpoints": [
        {
          "latitude": 29.605806, 
          "longitude": -98.532033, 
          "name": "Well Tree", 
          "pk": 3, 
          "type": "XMASTREE"
        }, 
        {
          "latitude": 29.605753, 
          "longitude": -98.532306, 
          "name": "Tank 1", 
          "pk": 4, 
          "type": "TANK"
        }, 
        {
          "latitude": 29.605647, 
          "longitude": -98.53229, 
          "name": "Tank 2", 
          "pk": 5, 
          "type": "TANK"
        }
      ], 
      "latitude": 29.605806, 
      "longitude": -98.532033, 
      "name": "Bob Well", 
      "pk": 1
    }
  ]
}
```

### Example #2 - Retrieve Site Details
Retreiving Site details for site #1.

`curl -i http://wellaware-frontend-dev-test.herokuapp.com/site/1`

Response:
```
{
  "endpoints": [
    {
      "latitude": 29.605806, 
      "longitude": -98.532033, 
      "name": "Well Tree", 
      "pk": 3, 
      "type": "XMASTREE"
    }, 
    {
      "latitude": 29.605753, 
      "longitude": -98.532306, 
      "name": "Tank 1", 
      "pk": 4, 
      "type": "TANK"
    }, 
    {
      "latitude": 29.605647, 
      "longitude": -98.53229, 
      "name": "Tank 2", 
      "pk": 5, 
      "type": "TANK"
    }
  ], 
  "latitude": 29.605806, 
  "longitude": -98.532033, 
  "name": "Bob Well", 
  "pk": 1, 
  "today_gas_production": 246.20786967993388, 
  "today_oil_production": 48.53745999621435, 
  "today_water_production": 36.7658882830496
}
```

### Example #3 - Retrieve Asset Details
Retreiving asset endpoint details for endpoint #1.

`curl -i http://wellaware-frontend-dev-test.herokuapp.com/endpoint/1`

Response:
```
{
  "latitude": 29.603376, 
  "longitude": -98.529973, 
  "name": "Tank 1", 
  "pk": 1, 
  "today_oil_production": 91.71800046904869, 
  "today_water_production": 8.498361102556451, 
  "type": "TANK"
}
```

### Example #4 - Retrieve Asset Historical Data
Retreiving Historical data from endpoint #1. This data should be plotted on a graph. Assume Data starts from current time and is 30 seconds between each measurement back in time. Ie. the first data point has time `now`, the second data point in the array has a time of `now`- 30 seconds, etc.

`curl -i http://wellaware-frontend-dev-test.herokuapp.com/endpoint/1/data`

Response:
```
{
  "oil_production": [
    108.49231794796216, 
    95.9313379619684, 
    102.71500047520455, 
    94.48776071662176,
    ...,
    98.2653144027087, 
    99.97458289909677, 
    97.87445812692033, 
    110.79417149496166
  ], 
  "water_production": [
    24.25093342579036, 
    37.39679490016338, 
    34.98505935765509, 
    42.99026048927668, 
    ...,
    111.77708298334879, 
    102.59466008194757, 
    99.58665836962413, 
    102.7233176045807
  ]
}
```
