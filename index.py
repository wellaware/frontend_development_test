import os
from datetime import timedelta
from flask import Flask, Response, jsonify, request, make_response, current_app
from functools import wraps, update_wrapper
from settings import *
from random import random
from math import sin
from copy import deepcopy

app = Flask(__name__)
app.debug = DEBUG


def crossdomain(origin=None, methods=None, headers=None, max_age=21600, attach_to_all=True, automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator


def jsonp(func):
    """Wraps JSONified output for JSONP requests."""
    @wraps(func)
    def decorated_function(*args, **kwargs):
        callback = request.args.get('callback', False)
        if callback:
            data = str(func(*args, **kwargs).data)
            content = str(callback) + '(' + data + ')'
            mimetype = 'application/javascript'
            return current_app.response_class(content, mimetype=mimetype)
        else:
            return func(*args, **kwargs)
    return decorated_function


@app.errorhandler(404)
@jsonp
def not_found(error):
    return make_response(jsonify({'error': 'Not Found'}), 404)


@app.route('/sites')
@crossdomain(origin='*')
@jsonp
def sites():
    return jsonify({'sites': SITES})


@app.route('/site/<int:site_id>')
@crossdomain(origin='*')
@jsonp
def site_view(site_id):
    for site in SITES:
        if site['pk'] == site_id:
            tsite = deepcopy(site)
            tsite['today_oil_production'] = 100.0*random()
            tsite['today_water_production'] = 100.0*random()
            tsite['today_gas_production'] = 300.0*random()
            return jsonify(tsite)
    return make_response(jsonify({'error': 'Not found.'}), 404)


@app.route('/endpoint/<int:endpoint_id>')
@crossdomain(origin='*')
@jsonp
def endpoint_view(endpoint_id):
    for site in SITES:
        for endpoint in site['endpoints']:
            if endpoint['pk'] == endpoint_id:
                tendpoint = deepcopy(endpoint)
                if endpoint['type'] == 'TANK':
                    tendpoint['today_oil_production'] = 100.0*random()
                    tendpoint['today_water_production'] = 100.0*random()
                else:
                    tendpoint['today_gas_production'] = 300.0*random()
                return jsonify(tendpoint)
    return make_response(jsonify({'error': 'Not found.'}), 404)


@app.route('/endpoint/<int:endpoint_id>/data')
@crossdomain(origin='*')
@jsonp
def endpoint_data_view(endpoint_id):
    for site in SITES:
        for endpoint in site['endpoints']:
            if endpoint['pk'] == endpoint_id:
                if endpoint['type'] == 'TANK':
                    startval1, startval2 = int(random()*628), int(random()*628)
                    endval1 = startval1 + 628 + 1
                    endval2 = startval2 + 528 + 1
                    return jsonify({'oil_production': [100.0*sin(i/100.)+150.0 + random()*20.0 for i in xrange(startval1, endval1)],
                                    'water_production': [100.0*sin(i/100.)+120.0 + random()*22.0 for i in xrange(startval2, endval2)]
                        })
                else:
                    startval = int(random()*628)
                    endval = startval + 628 + 1
                    return jsonify({'gas_production': [100.0*sin(i/100.)+150.0 + random()*20.0 for i in xrange(startval, endval)]})
    return make_response(jsonify({'error': 'Not found.'}), 404) 
